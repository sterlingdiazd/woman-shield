<?php
	
	class User {
		
		private $id;
		private $gcm_regid;
		private $nombre;
		private $apellido;
		private $correo;
		private $birthdate;
		private $cellphone;
		private $registeredDate;

		function __construct($gcm_regid, $nombre, $apellido, $correo, $birthdate, $cellphone, $registeredDate)
		{
			$this->gcm_regid = $gcm_regid;
			$this->nombre = (string) $nombre;
			$this->apellido = (string) $apellido;
			$this->correo = (string) $correo;
			$this->birthdate = (string) $birthdate;
			$this->cellphone = (int) $cellphone;
			$this->registeredDate = (string) $registeredDate;
		}
		
		public function  setId($id){
			$this->id = $id;
		}
		
		public function getId(){
			return $this->id;
		}
		
		public function  setGcm_regid($gcm_regid){
			$this->gcm_regid = $gcm_regid;
		}
		
		public function getGcm_regid(){
			return $this->gcm_regid;
		}
				
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		public function getNombre(){
			return $this->nombre;
		}
		
		public function getApellido(){
			return $this->apellido;
		}
		
		public function setApellido($apellido){
			$this->apellido = $apellido;
		}
		
		public function  setCorreo($correo){
			$this->correo = $correo;
		}
		
		public function getCorreo(){
			return $this->correo;
		}
		
		public function setBirthdate($birthdate){
			$this->birthdate = $birthdate;
		}
		
		public function getBirthdate(){
			return $this->birthdate;
		}
		
		public function setCellphone($cellphone){
			$this->cellphone = $cellphone;
		}
		
		public function getCellphone(){
			return $this->cellphone;
		}		
		
		public function setRegisteredDate($registeredDate){
			$this->registeredDate = $registeredDate;
		}
		
		public function getRegisteredDate(){
			return $this->registeredDate;
		}
		
	}
	
?>

