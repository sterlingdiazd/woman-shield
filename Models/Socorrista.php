<?php
	
	class Socorrista {
		
		private $id;
		private $nombre;
		private $apellido;
		private $correo;
		private $personal_id;
		private $birthdate;
		private $cellphone;
		private $location;
		private $relationship;
		
		function __construct($nombre, $apellido,  $personal_id,$correo, $birthdate, $cellphone, $location, $relationship)
		{
			$this->nombre = (string) $nombre;
			$this->apellido = (string) $apellido;
			$this->personal_id = (string) $personal_id;
			$this->correo = (string) $correo;
			$this->birthdate = (string) $birthdate;
			$this->cellphone = (string) $cellphone;
			$this->location = (string) $location;
			$this->relationship = (string) $relationship;
		}
		
		public function  setId($id){
			$this->id = $id;
		}
		
		public function getId(){
			return $this->id;
		}
				
		public function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		public function getNombre(){
			return $this->nombre;
		}
		
		public function getApellido(){
			return $this->apellido;
		}
		
		public function setApellido($apellido){
			$this->apellido = $apellido;
		}
		
		public function  setCorreo($correo){
			$this->correo = $correo;
		}
		
		public function getCorreo(){
			return $this->correo;
		}
		
		public function setPersonal_id($personal_id){
			$this->personal_id = $personal_id;
		}
		
		public function getPersonal_id(){
			return $this->personal_id;
		}
		
		public function setBirthdate($birthdate){
			$this->birthdate = $birthdate;
		}
		
		public function getBirthdate(){
			return $this->birthdate;
		}
		
		public function setCellphone($cellphone){
			$this->cellphone = $cellphone;
		}
		
		public function getCellphone(){
			return $this->cellphone;
		}		
		
		public function setLocation($location){
			$this->location = $location;
		}
		
		public function getLocation(){
			return $this->location;
		}
		
		public function setRelationship($relationship){
			$this->relationship = $relationship;
		}
		
		public function getRelationship(){
			return $this->relationship;
		}	
		
		
		
	}
	
?>

