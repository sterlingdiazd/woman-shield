<?php
	
	class User {
		
		private $id;
		private $frase;
		private $autor;
		private $id_usuario_publicador;
		private $fecha;
		private $hora;
		private $status;


 
		function __construct($id,		 $frase, $autor, $id_usuario_publicador,		 $fecha,		 $hora,		 $status)
		{
			$this->nombre = (string) $frase;
			$this->apellido = (string) $autor;
			$this->correo = (string) $id_usuario_publicador;
			$this->birthdate = (string) $fecha;
			$this->cellphone = (int) $hora;
			$this->status = (int) $status;
		}
		
		public function  setId($id){
			$this->id = $id;
		}
		
		public function getId(){
			return $this->id;
		}
				
		public function setFrase($frase){
			$this->frase = $frase;
		}
		
		public function getFrase(){
			return $this->frase;
		}
		
		public function getAutor(){
			return $this->autor;
		}
		
		public function setAutor($autor){
			$this->autor = $autor;
		}
		
		public function  setId_usuario_publicador($id_usuario_publicador){
			$this->id_usuario_publicador = $id_usuario_publicador;
		}
		
		public function getId_usuario_publicador(){
			return $this->id_usuario_publicador;
		}
		
		public function setFecha($fecha){
			$this->fecha = $fecha;
		}
		
		public function getFecha(){
			return $this->fecha;
		}
		
		
		public function setHora($hora){
			$this->hora = $hora;
		}
		
		public function getHora(){
			return $this->hora;
		}	
		
		public function setStatus($status){
			$this->status = $status;
		}
		
		public function getStatus(){
			return $this->status;
		}		
	}
	
?>

