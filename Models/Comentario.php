<?php
	
	class Comentario {
		
		private $id;
		private $comentario;
		private $idAutor;
		private $nombreAutor;
		private $fecha;
		private $hora;
		private $status;

		function __construct($id, $comentario, $idAutor, $nombreAutor, $fecha, $hora, $status)
		{
			$this->comentario = (string) $comentario;
			$this->idAutor = (string) $idAutor;
			$this->nombreAutor = (string) $nombreAutor;
			$this->fecha = (string) $fecha;
			$this->hora = (string) $hora;
			$this->status = (string) $status;
		}
		
		public function  setId($id){
			$this->id = $id;
		}
		
		public function getId(){
			return $this->id;
		}
				
		public function setComentario($comentario){
			$this->comentario = $comentario;
		}
		
		public function getComentario(){
			return $this->comentario;
		}
		
		public function getIdAutor(){
			return $this->idAutor;
		}
		
		public function setIdAutor($idAutor){
			$this->idAutor = $idAutor;
		}
		
		public function  setNombreAutor($nombreAutor){
			$this->nombreAutor = $nombreAutor;
		}
		
		public function getNombreAutor(){
			return $this->nombreAutor;
		}
		
		public function setFecha($fecha){
			$this->fecha = $fecha;
		}
		
		public function getFecha(){
			return $this->fecha;
		}
		
		
		public function setHora($hora){
			$this->hora = $hora;
		}
		
		public function getHora(){
			return $this->hora;
		}	
		
		public function setStatus($status){
			$this->status = $status;
		}
		
		public function getStatus(){
			return $this->status;
		}		
	}
	
?>

