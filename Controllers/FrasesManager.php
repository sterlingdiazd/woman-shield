<?php
	class FrasesManager 
	{
		public function registrarFrase($frase)
		{
			$query = "INSERT INTO frases_concientizacion 
			VALUES (
			'0',
			'{$frase->getFrase()}',
			'{$frase->getAutor()}',
			'{$frase->getId_usuario_publicador()}',
			'{$frase->getFecha()}',	
			'{$frase->getHora()}'	
			'{$frase->getStatus()}'
			)";
			
			
			if(mysql_query($query) == 1)
			{
				$data = array
				(
				'id' => mysql_insert_id(),
				'name' => $frase->getNombre(),
				'lastname' => $frase->getApellido(),
				'email' => $frase->getCorreo(),
				'birthdate' => $frase->getBirthdate(),	
				'cellphone' => $frase->getCellphone()
				); 
				
				
				print (json_encode($data)); 
				
			} else {
				return -1;	
			}
		}
		
	public function searchFrase($status)
	{
		$query = "SELECT * FROM frases_concientizacion WHERE status = '".$status."' ";
		
		$result = mysql_query($query);
		print( json_encode( mysql_fetch_assoc($result) ) );
			
	}
	
	public function searchAllComentarios($status)
	{
		$query = "SELECT * FROM comentarios WHERE status = '".$status."' ";
		
		$rows = array();
		$result = mysql_query($query);	
		while($row = mysql_fetch_assoc($result)) 
		{
			$rows[] = $row;
		}
		print( json_encode( $rows ) );
	}	
	
	
	public function insertarComentario($comentario)
	{
		$query = "INSERT INTO comentarios 
			VALUES (
			'0',
			'{$comentario->getComentario()}',
			'{$comentario->getIdAutor()}',
			'{$comentario->getNombreAutor()}',
			'{$comentario->getFecha()}',	
			'{$comentario->getHora()}',	
			'{$comentario->getStatus()}'
			)";
			
			if(mysql_query($query) == 1)
			{
				$data = array
				(
				'id' => mysql_insert_id(),
				'comentario' => $comentario->getComentario(),
				'idAutor' => $comentario->getIdAutor(),
				'nombreAutor' => $comentario->getNombreAutor(),
				'fecha' => $comentario->getFecha(),	
				'hora' => $comentario->getHora(),
				'status' => $comentario->getHora()
				); 
				
				print (json_encode($data)); 
				
			} else {
				return -1;	
			}
	}
	
	public function insertarSocorrista($socorrista)
	{
		$query = "INSERT INTO socorristas 
			VALUES (
			'0',
			'{$socorrista->getNombre()}',
			'{$socorrista->getApellido()}',
			'{$socorrista->getPersonal_id()}',
			'{$socorrista->getCorreo()}',			
			'{$socorrista->getBirthdate()}',	
			'{$socorrista->getCellphone()}',	
			'{$socorrista->getLocation()}',
			'{$socorrista->getRelationship()}'
			)";
			
			//echo $query;
			
			if(mysql_query($query) == 1)
			{
				$data = array
				(
				'id' => mysql_insert_id(),
				'name' => $socorrista->getNombre(),
				'lastname' => $socorrista->getApellido(),
				'personal_id' => $socorrista->getPersonal_id(),
				'email' => $socorrista->getCorreo(),	
				'birthdate' => $socorrista->getBirthdate(),
				'cellphone' => $socorrista->getCellphone(),
				'location' => $socorrista->getLocation(),	
				'relationship' => $socorrista->getRelationship(),
				); 
				
				print (json_encode($data)); 
				
			} else {
				return -1;	
			}
			
	}
	
	
	public function searchAllSocorristas()
	{
		$query = "SELECT * FROM socorristas";
		
		$rows = array();
		$result = mysql_query($query);	
		while($row = mysql_fetch_assoc($result)) 
		{
			$rows[] = $row;
		}
		print( json_encode( $rows ) );
	}
		
}
?>
