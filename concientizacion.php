
<?php 

	include 'lib/Engine.php';	
	require_once("Controllers/FrasesManager.php");
	require_once("Models/Comentario.php");
	require_once("Models/Socorrista.php");
	
		
		$frasesManager = new FrasesManager();

		if( isset($_POST["action"]) )
		{
			$action = $_POST["action"];
		}	
		
		if($action == "searchFrase")
		{
			if( isset($_POST["status"]) )
			{
				$status = $_POST["status"];
			}

			echo '[';
			echo $frasesManager->searchFrase($status);
			echo ']';
				
		}
		
		if($action == "searchAllComentarios")
		{
			if( isset($_POST["status"]) )
			{
				$status = $_POST["status"];
			}
			
			echo '[';
			echo $frasesManager->searchAllComentarios($status);
			echo ']';
		}	
		
		
		if($action == "insertarComentario")
		{		
			if( isset($_POST["comentario"]) )
			{
				$comentario = $_POST["comentario"];
			}
			if( isset($_POST["idAutor"]) )
			{
				$idAutor = $_POST["idAutor"];
			}
			if( isset($_POST["nombreAutor"]) )
			{
				$nombreAutor = $_POST["nombreAutor"];
			}
			
			$fecha = date("Y-m-d");
			$hora = $today = date("H:i:s");
			
			$comment = new Comentario(0, $comentario, $idAutor, $nombreAutor, $fecha, $hora, "PENDING");
			
			echo '[';
			echo $frasesManager->insertarComentario($comment);
			echo ']';
		}	
		
		if($action == "searchAllSocorristas")
		{			
			echo '[';
			echo $frasesManager->searchAllSocorristas();
			echo ']';
		}	
		
		if($action == "insertarSocorrista")
		{		
			if( isset($_POST["name"]) )
			{
				$name= $_POST["name"];
			}
			if( isset($_POST["lastname"]) )
			{
				$lastname = $_POST["lastname"];
			}
			if( isset($_POST["personal_id"]) )
			{
				$personal_id = $_POST["personal_id"];
			}
			if( isset($_POST["email"]) )
			{
				$email = $_POST["email"];
			}
			if( isset($_POST["birthdate"]) )
			{
				$birthdate = $_POST["birthdate"];
			}
			if( isset($_POST["cellphone"]) )
			{
				$cellphone = $_POST["cellphone"];
			}
			if( isset($_POST["location"]) )
			{
				$location = $_POST["location"];
			}
			if( isset($_POST["relationship"]) )
			{
				$relationship = $_POST["relationship"];
			}
			
			$socorrista = new Socorrista($name, $lastname, $personal_id, $email, $birthdate, $cellphone, $location, $relationship);
			
			echo '[';
			echo $frasesManager->insertarSocorrista($socorrista);
			echo ']';
		}
		
?>